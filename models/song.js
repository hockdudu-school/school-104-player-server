module.exports = function (sequelize, DataTypes) {
    return sequelize.define('song', {
        name: {
            type: DataTypes.STRING(60),
            allowNull: false
        },
        name_friendly: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
        image: {
            type: DataTypes.STRING(80),
            allowNull: true
        },
        duration: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        play_count: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            defaultValue: 0
        },
        description: {
            type: DataTypes.STRING(1000),
            allowNull: true
        },
        location: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        release_date: {
            type: DataTypes.DATEONLY,
            allowNull: true
        },
        genre_custom: {
            type: DataTypes.STRING(20),
            allowNull: true
        },
    }, {
        tableName: 'song',
        underscored: true,
    });
};
