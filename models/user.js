module.exports = function (sequelize, DataTypes) {
    return sequelize.define('user', {
        name: {
            type: DataTypes.STRING(60),
            allowNull: false,
            unique: true
        },
        email: {
            type: DataTypes.STRING(60),
            allowNull: false,
            unique: true,
            isEmail: true
        },
        password: {
            type: DataTypes.STRING(60),
            allowNull: false
        }
    }, {
        tableName: 'user',
        underscored: true,
    });
};
