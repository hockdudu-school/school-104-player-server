module.exports = function (sequelize, DataTypes) {
    return sequelize.define('genre', {
        name: {
            type: DataTypes.STRING(20),
            allowNull: false
        }
    }, {
        tableName: 'genre',
        underscored: true,
    });
};
