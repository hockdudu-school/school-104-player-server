module.exports = function (sequelize, DataTypes) {
    return sequelize.define('tag', {
        name: {
            type: DataTypes.STRING(20),
            allowNull: false
        }
    }, {
        tableName: 'tag',
        underscored: true,
    });
};
