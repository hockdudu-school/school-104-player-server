module.exports = function (sequelize, DataTypes) {
    return sequelize.define('playlist', {
        name: {
            type: DataTypes.STRING(60),
            allowNull: false
        },
        name_friendly: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
        is_private: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
            validate: {
                isBoolean(val) {
                    if (typeof val !== 'boolean') {
                        throw new Error('Tried saving int on is_private')
                    }
                }
            }
        },
        is_album: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
            validate: {
                isBoolean(val) {
                    if (typeof val !== 'boolean') {
                        throw new Error('Tried saving int on is_album')
                    }
                }
            }
        },
        release_date: {
            type: DataTypes.DATEONLY,
            allowNull: true
        },
        genre_custom: {
            type: DataTypes.STRING(20),
            allowNull: true
        },
        description: {
            type: DataTypes.STRING(1000),
            allowNull: true
        }
    }, {
        tableName: 'playlist',
        underscored: true,
    });
};
