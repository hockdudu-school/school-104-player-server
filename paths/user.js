module.exports = (app, User, bcrypt, jwt) => {

    const Op = User.sequelize.Op;

    app.get('/user/id/:userId', async (req, res) => {
        let user = await User.findById(req.params.userId);

        if (user == null) {
            return res.status(404).end();
        }

        let userInfo = {
            id: user.id,
            name: user.name
        };

        res.send(userInfo);
    });

    app.get('/user/name/:userName', async (req, res) => {
        let user = await User.findOne({where: {name: req.params.userName}});

        if (user == null) {
            return res.status(404).end();
        }

        let userInfo = {
            id: user.id,
            name: user.name
        };

        res.send(userInfo);
    });

    app.post('/user/new', async (req, res) => {

        let name = req.body.name;
        let pass = req.body.password;
        let email = req.body.email;

        if (!name || !pass || !email)
            return res.status(400).end();

        let existingUser = await User.findOne({where: {[Op.or]: [{name: name}, {email: email}]}});

        if (existingUser != null) {
            return res.status(409).end();
        }

        let safePassword = await bcrypt.hash(pass, 10);

        let user = await User.create({
            name: name,
            email: email,
            password: safePassword
        });

        let expires = new Date();
        expires.setDate(expires.getDate() + 7);
        let expireTime = Math.round(expires.getTime() / 1000);

        let token = jwt.encode({
            iss: user.id,
            exp: expireTime
        }, app.get('jwtTokenSecret'));

        let userInfo = {
            id: user.id,
            name: user.name,
            token: token
        };

        res.send(userInfo);
    })
};