module.exports = (app, Tag, Song, Playlist, User, jwt) => {

    const authenticateUser = require('../lib/authenticateUser')(User, jwt);
    const Op = Tag.sequelize.Op;

    app.get('/tag/:tag', async (req, res) => {
        let foundTags = await Tag.findAll({
            where: {name: req.params.tag},
            include: [{model: Song}, {model: Playlist, where: {is_private: false}}]
        });

        if (foundTags == null) {
            return res.status(404).end();
        }

        res.send(foundTags);
    });

    app.post('/tag/:tag', async (req, res) => {
        let user;
        try {
            user = await authenticateUser(req.body.token);
        } catch (e) {
            return res.status(401).end();
        }

        let foundTags = await Tag.findAll({
            where: {name: req.params.tag},
            include: [{model: Song}, {
                model: Playlist,
                where: {[Op.or]: [{is_private: false}, {is_private: true, user_id: user.id}]}
            }]
        });

        if (foundTags == null) {
            return res.status(404).end();
        }

        res.send(foundTags);
    });
};



