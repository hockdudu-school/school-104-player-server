module.exports = (app, User, jwt, bcrypt) => {
    app.post('/auth/token', async (req, res) => {
        let username = req.body.name;
        let password = req.body.password;

        if (!username || !password) return res.status(401).end();

        let user = await User.findOne({where: {name: username}});

        if (!user) return res.status(401).send();

        let userHash = user.password;

        let isPassValid = await bcrypt.compare(password, userHash);

        if (!isPassValid) return res.status(401).end();


        let expires = new Date();
        expires.setDate(expires.getDate() + 7);

        let expireTime = Math.round(expires.getTime() / 1000);

        let token = jwt.encode({
            iss: user.id,
            exp: expireTime
        }, app.get('jwtTokenSecret'));

        res.send({
            id: user.id,
            name: user.name,
            token: token,
            expires: expireTime
        });
    });
};