module.exports = (app, Genre) => {
    app.get('/genre/:genreId?', async (req, res) => {
        let genre;
        if (req.params.genreId) {
            genre = await Genre.findById(req.params.genreId);
        } else {
            genre = await Genre.findAll();
        }

        if (!genre) res.status(404).end();
        else res.send(genre);
    });
};

