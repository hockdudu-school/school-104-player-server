const formidable = require('formidable');
const shortid = require('shortid');
const fs = require('fs');

const taskQueueTimeout = 5 * 60 * 1000;
let tasksQueue = [];

fs.access(process.env.FILE_UPLOAD_PATH, fs.constants.F_OK, err => {
    if (err)
        fs.mkdir(process.env.FILE_UPLOAD_PATH, err => {
            if (err)
                throw new Error(`Couldn't create folder ${process.env.FILE_UPLOAD_PATH}, ${err}`)
        });
});

const deleteFilesArray = (filesArray) => {
    for (let file of filesArray) {
        if (!file) continue;
        fs.access(file, fs.constants.F_OK, err => {
            if (!err)
                fs.unlink(file, err => {
                    if (err)
                        console.log(`Couldn't delete file ${file}, ${err}`);
                });
        });
    }
};

const processImage = require('../lib/processImage')();
const processSong = require('../lib/processSong')();

const tasksHandler = {
    newTask: function (promise, completionCallBack, errorCallBack) {
        let generatedTaskId = shortid.generate();

        promise
            .then(returnArray => {
                completionCallBack(returnArray);
            })
            .catch(error => {
                errorCallBack(error);
            });

        tasksQueue[generatedTaskId] = {code: 202, body: null, header: {}};

        return generatedTaskId;
    },
    getTaskStatus: function (taskId) {
        if (tasksQueue.hasOwnProperty(taskId)) {
            return tasksQueue[taskId];
        } else {
            return null;
        }
    },
    finalizeTask: function (taskId, code, response, header) {
        if (tasksQueue.hasOwnProperty(taskId)) {

            tasksQueue[taskId] = {code: code, body: response, header: header};

            setTimeout(() => {
                delete tasksQueue[taskId];
            }, taskQueueTimeout)
        }
    }
};

module.exports = (app, Song, User, Tag, Genre, jwt) => {

    const authenticateUserWithToken = require('../lib/authenticateUser')(User, jwt);

    app.get('/song/id/:songId', async (req, res) => {
        let song = await Song.findById(req.params.songId, {include: [Tag]});

        if (song == null) {
            return res.status(404).end();
        }


        res.send(song);
    });

    app.get('/song/name/:userName/:songName?', async (req, res) => {
        let user = await User.findOne({where: {name: req.params.userName}});

        if (user == null) {
            return res.status(404).end();
        }

        let songs;
        if (!req.params.songName) {
            songs = await user.getSongs({include: [Tag]});
        } else {
            songs = await user.getSongs({where: {name: req.params.songName}, include: [Tag]});
        }

        if (songs == null || req.params.songName && songs.length === 0) {
            return res.status(404).end();
        }

        if (req.params.songName) res.send(songs[0]);
        else res.send(songs);
    });

    app.post('/song/upload', async (req, res) => {
        let form = formidable.IncomingForm();
        form.uploadDir = process.env.FILE_UPLOAD_TEMP_PATH;
        form.keepExtensions = true;
        form.type = 'multipart';

        let json;
        let userInfo;

        let uploadedFile;
        let image;

        let randomName = shortid.generate();

        const deleteTempFile = (returnStatus, text) => {
            let riskArray = [
                uploadedFile ? uploadedFile.path : null,
                image ? image.path : null,
                `${process.env.FILE_UPLOAD_PATH}/${randomName}.oga`,
                `${process.env.FILE_UPLOAD_PATH}/${randomName}.jpg`
            ];

            deleteFilesArray(riskArray);

            if (!returnStatus) return; // No response needed

            if (text)
                res.status(returnStatus).send(text);
            else res.status(returnStatus).end();
        };

        form.on('field', async function (name, value) {
            if (name !== 'info') return;

            try {
                json = JSON.parse(value);
            } catch (e) {
                return deleteTempFile(400)
            }

            userInfo = authenticateUserWithToken(json.token);
            userInfo.catch(() => {
                deleteTempFile(401);
            });
        });

        form.on('fileBegin', function (name, file) {
            switch (name) {
                case "file":
                case "song":
                    if (uploadedFile) {
                        deleteTempFile(400);
                        return;
                    }
                    uploadedFile = file;
                    break;
                case "image":
                case "cover":
                    if (image) {
                        deleteTempFile(400);
                        return;
                    }
                    image = file;
                    break;
            }
        });

        form.on('end', async function () {

            try {
                userInfo = await userInfo;
            } catch (e) {
                return deleteTempFile(401)
            }

            if (!userInfo|| !uploadedFile || !json.name) {
                deleteTempFile(400);
                return;
            }

            let existentSong = await Song.findOne({where: {name: json.name, user_id: userInfo.id}});
            if (existentSong) {
                deleteTempFile(409);
                return;
            }

            let oldSongPath = uploadedFile.path;
            let newSongPath = `${randomName}.oga`;
            let songFSPath = `${process.env.FILE_UPLOAD_PATH}/${newSongPath}`;
            let ffmpegPromise = processSong(oldSongPath, songFSPath);


            let oldImagePath = image ? image.path : null;
            let newImagePath = `${randomName}.jpg`;
            let imageFSPath = `${process.env.FILE_UPLOAD_PATH}/${newImagePath}`;
            let gmPromise = image ? processImage(oldImagePath, imageFSPath) : Promise.resolve();

            let tags = [];
            if (typeof json.tags === 'object') {
                for (let tag of json.tags) {
                    tags.push(Tag.create({
                        name: tag
                    }));
                }
            }

            let genre = null;
            if (json.genre_id) {
                genre = await Genre.findById(json.genre_id);
                if (!genre) deleteTempFile(400, 'Invalid genre ID')
            }

            let taskId = tasksHandler.newTask(Promise.all([ffmpegPromise, gmPromise]), () => {
                fs.unlink(oldSongPath, err => {
                    if (err)
                        console.log(`Couldn't delete file ${oldSongPath}`);
                });

                if (image) {
                    fs.unlink(oldImagePath, err => {
                        if (err)
                            console.log(`Couldn't delete file ${oldImagePath}`);
                    });
                }

                let song = Song.create({
                    name: json.name,
                    name_friendly: json.name_friendly,
                    description: json.description,
                    release_date: json.release_date,
                    genre_custom: json.genre_custom,
                    image: image ? newImagePath : null,
                    location: newSongPath,
                    user_id: userInfo.id
                });

                song.then(song => {

                    song.setGenre(genre);

                    Promise.all(tags).then(tags => {
                        song.addTags(tags);
                    });

                    tasksHandler.finalizeTask(taskId, 303, null, {'Location': `/song/id/${song.id}`});
                }).catch(() => {
                    deleteTempFile();
                    tasksHandler.finalizeTask(taskId, 400, "Invalid song parameters");
                });
            }, err => {
                console.log(err);
                deleteTempFile();
                tasksHandler.finalizeTask(taskId, 500, "Error when processing audio and/or image");
            });

            res.status(201).send({task_id: taskId});
        });

        form.parse(req);

    });

    app.get('/song/queue/:queueId', async (req, res) => {
        let task = tasksHandler.getTaskStatus(req.params.queueId);
        if (task) {
            res.status(task.code).set(task.header).send(task.body);
        } else {
            res.status(410).end();
        }
    });

    app.delete('/song/id/:songId', async (req, res) => {
        let userInfo;
        try {
            let token = req.body.token;
            userInfo = await authenticateUserWithToken(token);
        } catch (e) {
            res.status(401).end();
        }

        let song = Song.findById(req.params.songId);

        song.then(song => {
            if (song.user_id !== userInfo.id)
                return res.status(403).end();

            let songFilePath = `${process.env.FILE_UPLOAD_PATH}/${song.location}`;
            fs.unlink(songFilePath, (err) => {
                if (err)
                    console.log(`Couldn't delete file ${songFilePath}, ${err}`);
            });

            if (song.image) {
                let imageFilePath = `${process.env.FILE_UPLOAD_PATH}/${song.image}`;
                fs.unlink(imageFilePath, (err) => {
                    if (err)
                        console.log(`Couldn't delete file ${imageFilePath}, ${err}`);
                });
            }


            song.destroy().then(() => {
                res.status(200).end();
            }).catch(err => {
                console.log(`Couldn't delete song, ${err}`);
                res.status(500).end();
            });
        }).catch(() => {
            res.status(404).end();
        });
    });

    app.patch('/song/id/:songId', async (req, res) => {
        let userInfo;

        if (!req.body.info) res.status(400).end();
        let newSongInfo = req.body.info;

        try {
            let token = req.body.token;
            userInfo = await authenticateUserWithToken(token);
        } catch (e) {
            res.status(401).end();
        }

        let song = Song.findById(req.params.songId);

        song.then(song => {
            if (song.user_id !== userInfo.id)
                return res.status(403).end();


            let options = {
                name: newSongInfo.hasOwnProperty('name') ? newSongInfo.name : song.name,
                name_friendly: newSongInfo.hasOwnProperty('name_friendly') ? newSongInfo.name_friendly : song.name_friendly,
                description: newSongInfo.hasOwnProperty('description') ? newSongInfo.description : song.description,
                release_date: newSongInfo.hasOwnProperty('release_date') ? newSongInfo.release_date : song.release_date,
                genre_custom: newSongInfo.hasOwnProperty('genre_custom') ? newSongInfo.genre_custom : song.genre_custom,
            };

            song = song.update(options);


            let tagsPromise;
            let updateTags = typeof newSongInfo.tags === 'object';
            if (updateTags) {
                tagsPromise = new Promise((resolve, reject) => {
                    let tags = [];
                    for (let tag of newSongInfo.tags) {
                        tags.push(Tag.create({
                            name: tag
                        }));
                    }

                    song.getTags().then(tags => {
                        for (let tag of tags) {
                            tag.destroy();
                        }
                    });

                    Promise.all(tags)
                        .then(tags => {
                            song.setTags(tags).then(() => {
                                resolve();
                            }).catch(err => {
                                console.log(`Error when setting song tags, ${err}`);
                                reject(err);
                            });
                        }).catch(err => {
                        reject(err);
                    });
                });
            } else {
                tagsPromise = Promise.resolve();
            }

            let genrePromise;
            let updateGenre = typeof newSongInfo.genre_id === 'number';
            if (updateGenre) {
                genrePromise = new Promise(async (resolve, reject) => {
                    let existingGenre = await Genre.findById(newSongInfo.genre_id);
                    if (!existingGenre)
                        return reject();

                    song.setGenre(newSongInfo.genre_id).then(() => {
                        resolve();
                    }).catch(err => {
                        console.log(`Error when setting song genre, ${err}`);
                        reject(err);
                    });
                });
            } else {
                genrePromise = Promise.resolve();
            }

            Promise.all([song, tagsPromise, genrePromise]).then(([song, tagsPromise, genrePromise]) => {
                song.reload({include: [Tag]}).then(song => {
                    res.send(song);
                }).catch(err => {
                    console.log(`Error when reloading song, ${err}`);
                    res.status(500).end();
                })
            }).catch(() => {
                res.status(400).end();
            });

        }).catch(() => {
            res.status(404).end();
        });
    });

    app.put('/song/id/:songId/image', async (req, res) => {
        let form = formidable.IncomingForm();
        form.uploadDir = process.env.FILE_UPLOAD_TEMP_PATH;
        form.keepExtensions = true;
        form.type = 'multipart';

        const deleteTempFile = (returnStatus, text) => {
            let riskArray = [
                image ? image.path : null,
                existingImageName ? `${process.env.FILE_UPLOAD_PATH}/${existingImageName}.jpg` : null
            ];

            deleteFilesArray(riskArray);

            if (text)
                res.status(returnStatus).send(text);
            else res.status(returnStatus).end();
        };

        let image;
        let token;

        let existingImageName;

        form.on('field', async function (name, value) {
            if (name !== 'token') return;

            try {
                token = authenticateUserWithToken(value);
            } catch (e) {
                deleteFilesArray(401);
            }
        });

        form.on('file', function (name, file) {
            switch (name) {
                case "file":
                case "image":
                case "cover":
                    if (image) {
                        deleteTempFile(400);
                        return;
                    }
                    image = file;
                    break;
            }
        });

        form.on('end', async function () {
            if (!image) return deleteTempFile(400);
            if (!token) return deleteTempFile(401);

            try {
                token = await token;
            } catch (e) {
                deleteTempFile(401)
            }

            let song = await Song.findById(req.params.songId);
            if (!song) return deleteTempFile(404);
            if (song.user_id !== token.id) return deleteTempFile(403);

            let uploadedImageOldPath = image.path;
            let newImageRelativePath;
            let newImagePath;

            if (song.image) {
                newImagePath = `${process.env.FILE_UPLOAD_PATH}/${song.image}`;
                newImageRelativePath = song.image;
            } else {
                let songPath = song.location;
                let strippedSongPath = /^(.+)\..*$/g.exec(songPath);
                newImagePath = `${process.env.FILE_UPLOAD_PATH}/${strippedSongPath[1]}.jpg`;
                newImageRelativePath = `${strippedSongPath[1]}.jpg`;
            }

            let gmImage = processImage(uploadedImageOldPath, newImagePath, newImageRelativePath);

            let taskHandlerId = tasksHandler.newTask(gmImage, () => {
                song.update({
                    image: newImageRelativePath
                }).then(() => {
                    tasksHandler.finalizeTask(taskHandlerId, 303, null, {'Location': `/song/id/${song.id}`});
                }).catch(err => {
                    //This shouldn't happen
                    tasksHandler.finalizeTask(taskHandlerId, 500);
                    console.log(`Error when updating database, song image update, ${err}`);
                });
                deleteFilesArray([uploadedImageOldPath]);
            }, error => {
                console.log(`Error when converting image, ${error}`);
                tasksHandler.finalizeTask(taskHandlerId, 500, "Error when converting image");
            });

            res.status(201).send({task_id: taskHandlerId});
        });

        form.parse(req);
    });

    app.delete('/song/id/:songId/image', async (req, res) => {
        let userInfo;
        try {
            let token = req.body.token;
            userInfo = await authenticateUserWithToken(token);
        } catch (e) {
            res.status(401).end();
        }

        let song = await Song.findById(req.params.songId);

        if (userInfo.id !== song.user_id) {
            return res.status(403).end();
        }

        let imageToDelete = `${process.env.FILE_UPLOAD_PATH}/${song.image}`;

        fs.unlink(imageToDelete, err => {
            if (err)
                console.log(`Error when deleting image ${imageToDelete}, ${err}`);
        });

        song.update({
            image: null
        }).then(() => {
            res.status(200).end();
        }).catch(err => {
            console.log(`Error when deleting song image from database, ${err}`);
        });
    });

};