module.exports = (app, Playlist, Song, User, Tag, Genre, jwt) => {

    const authenticateUserWithToken = require('../lib/authenticateUser')(User, jwt);

    app.get('/set/id/:setId', async (req, res) => {
        let set = await Playlist.findById(req.params.setId, {include: [{model: Song, include: [Tag]}, Tag]});
        if (set == null) {
            return res.status(404).end();
        }

        if (set.is_private)
            return res.status(401).end();

        res.send(set);
    });

    app.post('/set/id/:setId', async (req, res) => {
        let user;
        try {
            user = await authenticateUserWithToken(req.body.token);
        } catch (e) {
            return res.status(401).end();
        }

        let set = await Playlist.findById(req.params.setId, {include: [{model: Song, include: [Tag]}, Tag]});
        if (set == null) {
            return res.status(404).end();
        }

        if (!set.is_private) {
            res.send(set);

        } else {
            if (set.user_id === user.id) {
                res.send(set);
            } else {
                res.status(403).end();
            }
        }

    });

    app.get('/set/name/:userName/:setName?', async (req, res) => {
        let user = await User.findOne({where: {name: req.params.userName}});

        if (user == null) {
            return res.status(404).end();
        }

        let sets;
        if (!req.params.setName) {
            sets = await user.getPlaylists({where: {is_private: false}});
        } else {
            sets = await user.getPlaylists({
                where: {name: req.params.setName},
                include: [{model: Song, include: [Tag]}, Tag]
            });
        }

        if (sets == null || req.params.setName && sets.length === 0) {
            return res.status(404).end();
        }

        if (req.params.setName) {
            if (sets[0].is_private) {
                res.status(401).end();
            } else {
                res.send(sets[0]);
            }
        } else {
            res.send(sets);
        }
    });

    app.post('/set/name/:userName/:setName?', async (req, res) => {
        let tokenOwner;
        try {
            tokenOwner = await authenticateUserWithToken(req.body.token);
        } catch (e) {
            return res.status(401).end();
        }


        let user = await User.findOne({where: {name: req.params.userName}});
        if (user == null) {
            return res.status(404).end();
        }

        let sets;

        if (tokenOwner.id === user.id) {
            if (!req.params.setName) {
                sets = await user.getPlaylists();
            } else {
                sets = await user.getPlaylists({
                    where: {name: req.params.setName},
                    include: [{model: Song, include: [Tag]}, Tag]
                });
            }
        } else {
            if (!req.params.setName) {
                sets = await user.getPlaylists({where: {is_private: false}});
            } else {
                sets = await user.getPlaylists({
                    where: {name: req.params.setName},
                    include: [{model: Song, include: [Tag]}, Tag]
                });
                if (sets && sets[0].is_private) {
                    return res.status(403).end();
                }
            }
        }


        if (sets == null || req.params.setName && sets.length === 0) {
            return res.status(404).end();
        }

        if (req.params.setName) {
            res.send(sets[0]);
        } else {
            res.send(sets);
        }
    });

    app.post('/set/new', async (req, res) => {
        let userToken = req.body.token;
        let setInfo = req.body.info;
        let user;

        try {
            user = await authenticateUserWithToken(userToken);
        } catch (e) {
            return res.status(401).end();
        }

        if (!setInfo.name) return res.status(400).send("Invalid set name");

        let existentPlaylist = await Playlist.findOne({where: {user_id: user.id, name: setInfo.name}});

        if (existentPlaylist) return res.status(409).end();


        let genre = null;

        if (setInfo.genre_id) {
            genre = await Genre.findById(setInfo.genre_id);

            if (!genre) return res.status(400).send("Invalid genre ID");
        }

        let playlist;
        try {
            playlist = await Playlist.create({
                user_id: user.id,
                name: setInfo.name,
                name_friendly: setInfo.name_friendly,
                description: setInfo.description,
                is_album: setInfo.is_album,
                is_private: setInfo.is_private,
                release_date: setInfo.release_date,
                genre_custom: setInfo.genre_custom
            });
        } catch (e) {
            return res.status(400).end();
        }


        let areSongsSet = typeof setInfo.songs === 'object';
        let songsPromise;
        if (areSongsSet) {
            let songList = [];

            for (let songId of setInfo.songs) {
                songList.push(Song.findById(songId));
            }
            songsPromise = new Promise((resolve, reject) => {
                Promise.all(songList).then(resolved => {
                    playlist.addSongs(resolved)
                        .then(() => resolve())
                        .catch(err => reject(err));
                }).catch(err => reject(err));
            });
        } else {
            songsPromise = Promise.resolve();
        }

        let areTagsSet = typeof setInfo.tags === 'object';
        let tagsPromise;
        if (areTagsSet) {
            let tagsList = [];

            for (let tagName of setInfo.tags) {
                let tag = Tag.create({
                    name: tagName
                });
                tagsList.push(tag);
            }

            tagsPromise = new Promise((resolve, reject) => {
                Promise.all(tagsList).then(resolved => {
                    playlist.addTags(resolved)
                        .then(() => resolve())
                        .catch(err => reject(err));
                }).catch(err => reject(err));
            });

        } else {
            tagsPromise = Promise.resolve();
        }

        let genrePromise;
        if (genre) {
            genrePromise = playlist.setGenre(genre);
        } else {
            genrePromise = Promise.resolve();
        }

        Promise.all([songsPromise, tagsPromise, genrePromise]).then(() => {
            playlist.reload({include: [{model: Song, include: [Tag]}, Tag]}).then(playlist => {
                res.send(playlist);
            }).catch(err => {
                console.log(`Error when reloading playlist, ${err}`);
                res.status(500).end();
            });
        }).catch(() => {
            playlist.destroy().catch(err => {
                console.log(`Error when deleting playlist, ${err}`);
            });
            res.status(400).end();
        });
    });

    app.delete('/set/id/:setId', async (req, res) => {
        let userInfo;
        try {
            userInfo = await authenticateUserWithToken(req.body.token)
        } catch (e) {
            return res.status(401).end();
        }

        let playlist = Playlist.findById(req.params.setId);

        playlist.then(playlist => {
            if (playlist.user_id !== userInfo.id)
                return res.status(403).end();

            playlist.destroy().then(() => {
                res.status(200).end();
            }).catch(err => {
                console.log(`Error when deleting playlist, ${err}`);
                res.status(500).end();
            });
        }).catch(() => {
            res.status(404).end();
        });
    });

    app.patch('/set/id/:setId', async (req, res) => {
        let userInfo;
        if (!req.body.info) return res.send(400).end();
        let newPlaylistInfo = req.body.info;

        try {
            userInfo = await authenticateUserWithToken(req.body.token);
        } catch (e) {
            return res.status(401).end();
        }

        let playlist = Playlist.findById(req.params.setId);

        playlist.then(playlist => {
            if (playlist.user_id !== userInfo.id)
                return res.status(403).end();

            let options = {
                name: newPlaylistInfo.hasOwnProperty('name') ? newPlaylistInfo.name : playlist.name,
                name_friendly: newPlaylistInfo.hasOwnProperty('name_friendly') ? newPlaylistInfo.name_friendly : playlist.name_friendly,
                is_private: newPlaylistInfo.hasOwnProperty('is_private') ? newPlaylistInfo.is_private : playlist.is_private,
                is_album: newPlaylistInfo.hasOwnProperty('is_album') ? newPlaylistInfo.is_album : playlist.is_album,
                release_date: newPlaylistInfo.hasOwnProperty('release_date') ? newPlaylistInfo.release_date : playlist.release_date,
                genre_custom: newPlaylistInfo.hasOwnProperty('genre_custom') ? newPlaylistInfo.genre_custom : playlist.genre_custom,
                description: newPlaylistInfo.hasOwnProperty('description') ? newPlaylistInfo.description : playlist.description,
            };

            let playlistUpdatePromise = playlist.update(options);

            let tagsPromise;
            let updateTags = typeof newPlaylistInfo.tags === 'object';
            if (updateTags) {
                tagsPromise = new Promise((resolve, reject) => {
                    let tags = [];
                    for (let tag of newPlaylistInfo.tags) {
                        tags.push(Tag.create({
                            name: tag
                        }));
                    }

                    playlist.getTags().then(tags => {
                        for (let tag of tags) {
                            tag.destroy();
                        }
                    });

                    Promise.all(tags)
                        .then(tags => {
                            playlist.setTags(tags).then(() => {
                                resolve();
                            }).catch(err => {
                                console.log(`Error when setting playlist tags, ${err}`);
                                reject(err);
                            });
                        }).catch(err => {
                        reject(err);
                    });
                });
            } else {
                tagsPromise = Promise.resolve();
            }

            let genrePromise;
            let updateGenre = typeof newPlaylistInfo.genre_id === 'number';
            if (updateGenre) {
                genrePromise = new Promise(async (resolve, reject) => {
                    let existingGenre = await Genre.findById(newPlaylistInfo.genre_id);
                    if (!existingGenre)
                        return reject();

                    playlist.setGenre(newPlaylistInfo.genre_id).then(() => {
                        resolve();
                    }).catch(err => {
                        console.log(`Error when setting playlist genre, ${err}`);
                        reject(err);
                    });
                });
            } else {
                genrePromise = Promise.resolve();
            }

            Promise.all([playlistUpdatePromise, tagsPromise, genrePromise]).then(([playlist, tagsPromise, genrePromise]) => {
                playlist.reload({include: [Tag]}).then(() => {
                    res.send(playlist);
                }).catch(err => {
                    console.log(`Error when reloading playlist, ${err}`);
                    res.status(500).send();
                })
            }).catch(() => {
                res.status(400).end();
            });


        }).catch(() => {
            res.status(404).end();
        });
    });

    app.patch('/set/id/:setId/songs', async (req, res) => {
        let userInfo;
        if (!req.body.songs) return res.status(400).end();
        let newSongsList = req.body.songs;
        try {
            userInfo = await authenticateUserWithToken(req.body.token);
        } catch (e) {
            return res.status(401).end();
        }

        let playlist = Playlist.findById(req.params.setId);

        let songs = new Promise((resolve, reject) => {
            let songsArray = [];
            for (let songId of newSongsList) {
                songsArray.push(Song.findById(songId));
            }

            Promise.all(songsArray).then(songsArray => {
                resolve(songsArray);
            }).catch(err => {
                reject(err);
            });
        });

        playlist.then(playlist => {
            if (playlist.user_id !== userInfo.id)
                return res.status(403).end();

            songs.then(songsList => {
                playlist.setSongs(songsList).then(() => {
                    playlist.reload({include: [{model: Song, include: [Tag]}, Tag]}).then(() => {
                        res.send(playlist);
                    }).catch(err => {
                        console.log(`Error when reloading playlist, ${err}`);
                        res.status(500).end();
                    })
                }).catch(() => {
                    res.status(400).end();
                })
            }).catch(() => {
                res.status(400).end();
            });
        }).catch(() => {
            res.status(404).end();
        });
    });

    app.put('/set/id/:setId/songs', async (req, res) => {
        let userInfo;
        if (!req.body.songs) return res.status(400).end();
        let newSongsList = req.body.songs;
        try {
            userInfo = await authenticateUserWithToken(req.body.token);
        } catch (e) {
            return res.status(401).end();
        }

        let playlist = Playlist.findById(req.params.setId);

        let songs = new Promise((resolve, reject) => {
            let songsArray = [];
            for (let songId of newSongsList) {
                songsArray.push(Song.findById(songId));
            }

            Promise.all(songsArray).then(songsArray => {
                resolve(songsArray);
            }).catch(err => {
                reject(err);
            });
        });

        playlist.then(playlist => {
            if (playlist.user_id !== userInfo.id)
                return res.status(403).end();

            songs.then(songsList => {
                playlist.addSongs(songsList).then(() => {
                    playlist.reload({include: [{model: Song, include: [Tag]}, Tag]}).then(() => {
                        res.send(playlist);
                    }).catch(err => {
                        console.log(`Error when reloading playlist, ${err}`);
                        res.status(500).end();
                    })
                }).catch(() => {
                    res.status(400).end();
                })
            }).catch(() => {
                res.status(400).end();
            });
        }).catch(() => {
            res.status(404).end();
        });
    });

    app.delete('/set/id/:setId/songs', async (req, res) => {
        let userInfo;
        if (!req.body.songs) return res.status(400).end();
        let newSongsList = req.body.songs;
        try {
            userInfo = await authenticateUserWithToken(req.body.token);
        } catch (e) {
            return res.status(401).end();
        }

        let playlist = Playlist.findById(req.params.setId);

        let songs = new Promise((resolve, reject) => {
            let songsArray = [];
            for (let songId of newSongsList) {
                songsArray.push(Song.findById(songId));
            }

            Promise.all(songsArray).then(songsArray => {
                resolve(songsArray);
            }).catch(err => {
                reject(err);
            });
        });

        playlist.then(playlist => {
            if (playlist.user_id !== userInfo.id)
                return res.status(403).end();

            songs.then(songsList => {
                playlist.removeSongs(songsList).then(() => {
                    playlist.reload({include: [{model: Song, include: [Tag]}, Tag]}).then(() => {
                        res.send(playlist);
                    }).catch(err => {
                        console.log(`Error when reloading playlist, ${err}`);
                        res.status(500).end();
                    })
                }).catch(() => {
                    res.status(400).end();
                })
            }).catch(() => {
                res.status(400).end();
            });
        }).catch(() => {
            res.status(404).end();
        });
    });
};

