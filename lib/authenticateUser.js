module.exports = (User, jwt) => {
    return async (token) => {
        try {
            let userInfo = jwt.decode(token, process.env.JWT_SECRET);
            let existentUser = await User.findById(userInfo.iss);
            if (!existentUser) { // noinspection ExceptionCaughtLocallyJS
                throw new Error();
            }
            return existentUser;
        } catch (e) {
            throw new Error("Invalid token");
        }
    };
};
