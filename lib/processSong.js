const ffmpeg = require('fluent-ffmpeg');

module.exports = () => {
    return (originalSongPath, outputSongPath) => {
        return new Promise(((resolve, reject) => {
            ffmpeg(originalSongPath)
                .format('oga')
                .audioBitrate('128k')
                .audioChannels(2)
                .audioCodec('libopus')
                .on('error', err => {
                    reject(err);
                })
                .on('end', () => {
                    resolve();
                })
                .save(outputSongPath);
        }));
    }
};