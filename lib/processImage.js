const gm = require('gm').subClass({imageMagick: true});
gm.prototype.background = function (color) {
    this.out('-background', color);
    return this;
};


module.exports = () => {
    return async (oldImageAbsolutePath, newImageAbsolutePath) => {
        return new Promise((resolve, reject) => {
            gm(oldImageAbsolutePath)
                .resize(1024, 1024)
                .background('black')
                .compose('Copy')
                .gravity('Center')
                .extent(1024, 1024)
                .write(newImageAbsolutePath, err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    };
};