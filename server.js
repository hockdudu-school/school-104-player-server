require('dotenv').config();

const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DB_TABLE, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST,
    dialect: 'mysql',
    operatorsAliases: false,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },

    define: {
        timestamps: false
    },
});

const Genre = require('./models/genre')(sequelize, Sequelize);
const Playlist = require('./models/playlist')(sequelize, Sequelize);
const Song = require('./models/song')(sequelize, Sequelize);
const Tag = require('./models/tag')(sequelize, Sequelize);
const User = require('./models/user')(sequelize, Sequelize);

Playlist.belongsToMany(Song, {through: 'playlist_song', foreignKey: 'fk_playlist'});
Song.belongsToMany(Playlist, {through: 'playlist_song', foreignKey: 'fk_song'});

User.hasMany(Song, {foreignKey: {allowNull: false}, onDelete: 'CASCADE'});
User.hasMany(Playlist, {foreignKey: {allowNull: false}, onDelete: 'CASCADE'});

Song.belongsTo(Genre, {foreignKey: 'genre_id'});
Playlist.belongsTo(Genre, {foreignKey: 'genre_id'});

Song.hasMany(Tag, {onDelete: 'CASCADE'});
Tag.belongsTo(Song);

Playlist.hasMany(Tag, {onDelete: 'CASCADE'});
Tag.belongsTo(Playlist);

let databaseSync = sequelize.sync({force: JSON.parse(process.env.SERVER_RESET)});

const express = require('express');
const app = express();

let cors = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "content-type");
    res.header("Access-Control-Allow-Methods", "GET,POST,PATCH,PUT,DELETE");
    next();
};

app.use(cors);

app.use(express.json());

app.set('jwtTokenSecret', process.env.JWT_SECRET);

const bcrypt = require('bcrypt');
const jwt = require('jwt-simple');

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });


require('./paths/user')(app, User, bcrypt, jwt);

require('./paths/song')(app, Song, User, Tag, Genre, jwt);

require('./paths/set')(app, Playlist, Song, User, Tag, Genre, jwt);

require('./paths/tag')(app, Tag, Song, Playlist, User, jwt);

require('./paths/auth')(app, User, jwt, bcrypt);

require('./paths/genre')(app, Genre);

app.use('/file/', express.static(process.env.FILE_UPLOAD_PATH));


//
// TEST
//

app.get('/test/create', async (req, res) => {

    let genre1 = await Genre.create({
        name: 'tGenre1'
    });

    let genre2 = await Genre.create({
        name: 'tGenre2'
    });

    let user = await User.create({
        name: 'ttest-user',
        email: 'ttest@email.com',
        password: '$2b$10$Sy0a6O3qcpb9kJtO8ZBE5.1q8.JuHRZA/y4kPdTam0LPzmhtfXDt2' //pass
    });

    let song = await Song.create({
        name: 'ttest-song',
        name_friendly: 'Test Song',
        duration: 29,
        user_id: user.id,
        location: ''
    });

    song.setGenre(genre1);

    let song2 = await Song.create({
        name: 'ttest-song2',
        name_friendly: 'Test Song2',
        duration: 292,
        user_id: user.id,
        location: ''
    });

    let playlist = await Playlist.create({
        name: 'pplaylist-name',
        name_friendly: 'Playlist Name',
        user_id: user.id
    });

    playlist.addSong(song);
    playlist.addSong(song2);
    playlist.setGenre(genre2);

    let tag1 = await Tag.create({
        name: 'tag1'
    });

    let tag11 = await Tag.create({
        name: 'tag1'
    });

    let tag2 = await Tag.create({
        name: 'tag2'
    });

    song.addTag(tag11);
    playlist.addTags([tag1, tag2]);

    res.end();
});

app.get('/test/read', async (req, res) => {

    let playlist = await Playlist.findOne({where: {id: 1}, include: [{model: Song, include: [Genre]}]});

    res.send(playlist);
});


databaseSync.then(() => {
    app.listen(process.env.SERVER_PORT, () => {
        console.log('Express server started on port 3000')
    });
});
